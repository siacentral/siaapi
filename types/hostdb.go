package types

import (
	"time"
)

type (
	//HostDBEntry HostDBEntry
	HostDBEntry struct {
		HostExternalSettings
		FirstSeen                      uint64        `json:"firstseen"`
		LastHistoricUpdate             uint64        `json:"lasthistoricupdate"`
		HistoricDowntime               time.Duration `json:"historicdowntime"`
		HistoricUptime                 time.Duration `json:"historicuptime"`
		ScanHistory                    []HostDBScan  `json:"scanhistory"`
		HistoricFailedInteractions     float64       `json:"historicfailedinteractions"`
		HistoricSuccessfulInteractions float64       `json:"historicsuccessfulinteractions"`
		RecentFailedInteractions       float64       `json:"recentfailedinteractions"`
		RecentSuccessfulInteractions   float64       `json:"recentsuccessfulinteractions"`
		IPNets                         []string      `json:"ipnets"`
		LastIPNetChange                time.Time     `json:"lastipnetchange"`
		PublicKeyString                string        `json:"publickeystring"`
	}

	HostDBScan struct {
		Timestamp time.Time `json:"timestamp"`
		Success   bool      `json:"success"`
	}

	HostScoreBreakdown struct {
		Score                      BigNumber `json:"score"`
		ConversionRate             float64   `json:"conversionrate"`
		AgeAdjustment              float64   `json:"ageadjustment"`
		BurnAdjustment             float64   `json:"burnadjustment"`
		CollateralAdjustment       float64   `json:"collateraladjustment"`
		InteractionAdjustment      float64   `json:"interactionadjustment"`
		PriceAdjustment            float64   `json:"pricesmultiplier"`
		StorageRemainingAdjustment float64   `json:"storageremainingadjustment"`
		UptimeAdjustment           float64   `json:"uptimeadjustment"`
		VersionAdjustment          float64   `json:"versionadjustment"`
	}
)
