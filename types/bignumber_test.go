package types_test

import (
	"testing"

	"gitlab.com/siacentral/siaapi/types"
)

func TestBig(t *testing.T) {

}

func TestString(t *testing.T) {

}

func TestSetUint64(t *testing.T) {

}

func TestSetBig(t *testing.T) {

}

func TestCmp(t *testing.T) {

}

func TestCmp64(t *testing.T) {

}

func TestEquals(t *testing.T) {

}

func TestEquals64(t *testing.T) {

}

func TestAdd(t *testing.T) {
	t1 := new(types.BigNumber).SetUint64(100)
	t2 := new(types.BigNumber).SetUint64(50)

	t1.Add(t2)

	if t1.Cmp64(150) != 0 {
		t.Errorf("Expected %s got %s", "150", t1.String())
	}
}

func TestAddBig(t *testing.T) {

}

func TestAdd64(t *testing.T) {

}

func TestSub(t *testing.T) {
	t1 := new(types.BigNumber).SetUint64(100)
	t2 := new(types.BigNumber).SetUint64(50)

	t1.Sub(t2)

	if t1.Cmp64(50) != 0 {
		t.Errorf("Expected %s got %s", "50", t1.String())
	}
}

func TestSub64(t *testing.T) {

}

func TestMul(t *testing.T) {
	t1 := new(types.BigNumber).SetUint64(100)
	t2 := new(types.BigNumber).SetUint64(50)

	t1.Sub(t2)

	if t1.Cmp64(50) != 0 {
		t.Errorf("Expected %s got %s", "50", t1.String())
	}
}

func TestMul64(t *testing.T) {

}

func TestMulRat(t *testing.T) {

}

func TestDiv(t *testing.T) {

}

func TestDiv64(t *testing.T) {
	t1 := new(types.BigNumber).SetUint64(200)

	t1.Div64(2)

	if t1.Cmp64(100) != 0 {
		t.Errorf("Expected %s got %s", "100", t1.String())
	}
}

func TestExp(t *testing.T) {
	t1 := new(types.BigNumber).SetUint64(100)

	t1.Exp(2)

	if t1.Cmp64(10000) != 0 {
		t.Errorf("Expected %s got %s", "10000", t1.String())
	}
}

func TestSqrt(t *testing.T) {
	t1 := new(types.BigNumber).SetUint64(100)

	t1.Sqrt()

	if t1.Cmp64(10) != 0 {
		t.Errorf("Expected %s got %s", "10", t1.String())
	}
}

func TestUnmarshalJSON(t *testing.T) {

}

func TestScan(t *testing.T) {

}
