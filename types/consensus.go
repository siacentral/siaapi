package types

// ConsensusBlocksTxn contains all fields of a Transaction and an
// additional ID field.
type ConsensusBlocksTxn struct {
	ID                    string                         `json:"id"`
	SiacoinInputs         []SiacoinInput                 `json:"siacoininputs"`
	SiacoinOutputs        []ConsensusBlocksSiacoinOutput `json:"siacoinoutputs"`
	FileContracts         []ConsensusBlocksFileContract  `json:"filecontracts"`
	FileContractRevisions []FileContractRevision         `json:"filecontractrevisions"`
	StorageProofs         []StorageProof                 `json:"storageproofs"`
	SiafundInputs         []SiafundInput                 `json:"siafundinputs"`
	SiafundOutputs        []ConsensusBlocksSiafundOutput `json:"siafundoutputs"`
	MinerFees             []BigNumber                    `json:"minerfees"`
	ArbitraryData         [][]byte                       `json:"arbitrarydata"`
	TransactionSignatures []TransactionSignature         `json:"transactionsignatures"`
}

// ConsensusBlocksFileContract contains all fields of a FileContract
// and an additional ID field.
type ConsensusBlocksFileContract struct {
	ID                 string                         `json:"id"`
	FileSize           uint64                         `json:"filesize"`
	FileMerkleRoot     string                         `json:"filemerkleroot"`
	WindowStart        uint64                         `json:"windowstart"`
	WindowEnd          uint64                         `json:"windowend"`
	Payout             BigNumber                      `json:"payout"`
	ValidProofOutputs  []ConsensusBlocksSiacoinOutput `json:"validproofoutputs"`
	MissedProofOutputs []ConsensusBlocksSiacoinOutput `json:"missedproofoutputs"`
	UnlockHash         string                         `json:"unlockhash"`
	RevisionNumber     uint64                         `json:"revisionnumber"`
}

// ConsensusBlocksSiacoinOutput contains all fields of a SiacoinOutput
// and an additional ID field.
type ConsensusBlocksSiacoinOutput struct {
	ID         string    `json:"id"`
	Value      BigNumber `json:"value"`
	UnlockHash string    `json:"unlockhash"`
}

// ConsensusBlocksSiafundOutput contains all fields of a SiafundOutput
// and an additional ID field.
type ConsensusBlocksSiafundOutput struct {
	ID         string    `json:"id"`
	Value      BigNumber `json:"value"`
	UnlockHash string    `json:"unlockhash"`
}
