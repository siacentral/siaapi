package types

type (
	// ExplorerBlock is a block with some extra information such as the id and
	// height. This information is provided for programs that may not be
	// complex enough to compute the ID on their own.
	ExplorerBlock struct {
		MinerPayoutIDs []string              `json:"minerpayoutids"`
		Transactions   []ExplorerTransaction `json:"transactions"`
		RawBlock       Block                 `json:"rawblock"`
	}

	// ExplorerTransaction is a transcation with some extra information such as
	// the parent block. This information is provided for programs that may not
	// be complex enough to compute the extra information on their own.
	ExplorerTransaction struct {
		ID             string      `json:"id"`
		Height         uint64      `json:"height"`
		Parent         string      `json:"parent"`
		RawTransaction Transaction `json:"rawtransaction"`

		SiacoinInputOutputs                      []SiacoinOutput   `json:"siacoininputoutputs"` // the outputs being spent
		SiacoinOutputIDs                         []string          `json:"siacoinoutputids"`
		FileContractIDs                          []string          `json:"filecontractids"`
		FileContractValidProofOutputIDs          [][]string        `json:"filecontractvalidproofoutputids"`          // outer array is per-contract
		FileContractMissedProofOutputIDs         [][]string        `json:"filecontractmissedproofoutputids"`         // outer array is per-contract
		FileContractRevisionValidProofOutputIDs  [][]string        `json:"filecontractrevisionvalidproofoutputids"`  // outer array is per-revision
		FileContractRevisionMissedProofOutputIDs [][]string        `json:"filecontractrevisionmissedproofoutputids"` // outer array is per-revision
		StorageProofOutputIDs                    [][]string        `json:"storageproofoutputids"`                    // outer array is per-payout
		StorageProofOutputs                      [][]SiacoinOutput `json:"storageproofoutputs"`                      // outer array is per-payout
		SiafundInputOutputs                      []SiafundOutput   `json:"siafundinputoutputs"`                      // the outputs being spent
		SiafundOutputIDs                         []string          `json:"siafundoutputids"`
		SiafundClaimOutputIDs                    []string          `json:"siafundclaimoutputids"`
	}
)
