package types

type (
	SiacoinInput struct {
		ParentID         string           `json:"parentid"`
		UnlockConditions UnlockConditions `json:"unlockconditions"`
	}

	//SiacoinOutput SiacoinOutput
	SiacoinOutput struct {
		Value      BigNumber `json:"value"`
		UnlockHash []byte    `json:"unlockhash"`
	}

	SiafundInput struct {
		ParentID         string           `json:"parentid"`
		UnlockConditions UnlockConditions `json:"unlockconditions"`
		ClaimUnlockHash  string           `json:"claimunlockhash"`
	}

	SiafundOutput struct {
		Value      BigNumber `json:"value"`
		UnlockHash string    `json:"unlockhash"`
		ClaimStart BigNumber `json:"claimstart"`
	}

	ProcessedInput struct {
		ParentID       string    `json:"parentid"`
		FundType       [16]byte  `json:"fundtype"`
		WalletAddress  bool      `json:"walletaddress"`
		RelatedAddress string    `json:"relatedaddress"`
		Value          BigNumber `json:"value"`
	}

	ProcessedOutput struct {
		ID             string    `json:"id"`
		FundType       [16]byte  `json:"fundtype"`
		MaturityHeight uint64    `json:"maturityheight"`
		WalletAddress  bool      `json:"walletaddress"`
		RelatedAddress string    `json:"relatedaddress"`
		Value          BigNumber `json:"value"`
	}

	ProcessedTransaction struct {
		Transaction           Transaction `json:"transaction"`
		TransactionID         string      `json:"transactionid"`
		ConfirmationHeight    uint64      `json:"confirmationheight"`
		ConfirmationTimestamp Timestamp   `json:"confirmationtimestamp"`

		Inputs  []ProcessedInput  `json:"inputs"`
		Outputs []ProcessedOutput `json:"outputs"`
	}

	UnspentOutput struct {
		ID                 string    `json:"id"`
		FundType           [16]byte  `json:"fundtype"`
		UnlockHash         string    `json:"unlockhash"`
		Value              BigNumber `json:"value"`
		ConfirmationHeight uint64    `json:"confirmationheight"`
		IsWatchOnly        bool      `json:"iswatchonly"`
	}

	TransactionSignature struct {
		ParentID       string        `json:"parentid"`
		PublicKeyIndex uint64        `json:"publickeyindex"`
		Timelock       uint64        `json:"timelock"`
		CoveredFields  CoveredFields `json:"coveredfields"`
		Signature      []byte        `json:"signature"`
	}

	Transaction struct {
		SiacoinInputs         []SiacoinInput         `json:"siacoininputs"`
		SiacoinOutputs        []SiacoinOutput        `json:"siacoinoutputs"`
		FileContracts         []FileContract         `json:"filecontracts"`
		FileContractRevisions []FileContractRevision `json:"filecontractrevisions"`
		StorageProofs         []StorageProof         `json:"storageproofs"`
		SiafundInputs         []SiafundInput         `json:"siafundinputs"`
		SiafundOutputs        []SiafundOutput        `json:"siafundoutputs"`
		MinerFees             []BigNumber            `json:"minerfees"`
		ArbitraryData         [][]byte               `json:"arbitrarydata"`
		TransactionSignatures []TransactionSignature `json:"transactionsignatures"`
	}
)
