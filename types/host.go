package types

type (
	//HostFinancialMetrics HostFinancialMetrics
	HostFinancialMetrics struct {
		ContractCount                     uint64    `json:"contractcount"`
		ContractCompensation              BigNumber `json:"contractcompensation"`
		PotentialContractCompensation     BigNumber `json:"potentialcontractcompensation"`
		LockedStorageCollateral           BigNumber `json:"lockedstoragecollateral"`
		LostRevenue                       BigNumber `json:"lostrevenue"`
		LostStorageCollateral             BigNumber `json:"loststoragecollateral"`
		PotentialStorageRevenue           BigNumber `json:"potentialstoragerevenue"`
		RiskedStorageCollateral           BigNumber `json:"riskedstoragecollateral"`
		StorageRevenue                    BigNumber `json:"storagerevenue"`
		TransactionFeeExpenses            BigNumber `json:"transactionfeeexpenses"`
		DownloadBandwidthRevenue          BigNumber `json:"downloadbandwidthrevenue"`
		PotentialDownloadBandwidthRevenue BigNumber `json:"potentialdownloadbandwidthrevenue"`
		PotentialUploadBandwidthRevenue   BigNumber `json:"potentialuploadbandwidthrevenue"`
		UploadBandwidthRevenue            BigNumber `json:"uploadbandwidthrevenue"`
	}

	// HostInternalSettings contains a list of settings that can be changed.
	HostInternalSettings struct {
		AcceptingContracts        bool      `json:"acceptingcontracts"`
		MaxDownloadBatchSize      uint64    `json:"maxdownloadbatchsize"`
		MaxDuration               uint64    `json:"maxduration"`
		MaxReviseBatchSize        uint64    `json:"maxrevisebatchsize"`
		NetAddress                string    `json:"netaddress"`
		WindowSize                uint64    `json:"windowsize"`
		Collateral                BigNumber `json:"collateral"`
		CollateralBudget          BigNumber `json:"collateralbudget"`
		MaxCollateral             BigNumber `json:"maxcollateral"`
		MinContractPrice          BigNumber `json:"mincontractprice"`
		MinDownloadBandwidthPrice BigNumber `json:"mindownloadbandwidthprice"`
		MinStoragePrice           BigNumber `json:"minstorageprice"`
		MinUploadBandwidthPrice   BigNumber `json:"minuploadbandwidthprice"`
	}

	HostExternalSettings struct {
		AcceptingContracts     bool      `json:"acceptingcontracts"`
		MaxDownloadBatchSize   uint64    `json:"maxdownloadbatchsize"`
		MaxDuration            uint64    `json:"maxduration"`
		MaxReviseBatchSize     uint64    `json:"maxrevisebatchsize"`
		RemainingStorage       uint64    `json:"remainingstorage"`
		SectorSize             uint64    `json:"sectorsize"`
		TotalStorage           uint64    `json:"totalstorage"`
		NetAddress             string    `json:"netaddress"`
		UnlockHash             string    `json:"unlockhash"`
		WindowSize             uint64    `json:"windowsize"`
		RevisionNumber         uint64    `json:"revisionnumber"`
		BaseRPCPrice           BigNumber `json:"baserpcprice"`
		Collateral             BigNumber `json:"collateral"`
		MaxCollateral          BigNumber `json:"maxcollateral"`
		ContractPrice          BigNumber `json:"contractprice"`
		DownloadBandwidthPrice BigNumber `json:"downloadbandwidthprice"`
		SectorAccessPrice      BigNumber `json:"sectoraccessprice"`
		StoragePrice           BigNumber `json:"storageprice"`
		UploadBandwidthPrice   BigNumber `json:"uploadbandwidthprice"`
		Version                string    `json:"version"`
	}

	// HostNetworkMetrics reports the quantity of each type of RPC call that
	// has been made to the host.
	HostNetworkMetrics struct {
		DownloadCalls     uint64 `json:"downloadcalls"`
		ErrorCalls        uint64 `json:"errorcalls"`
		FormContractCalls uint64 `json:"formcontractcalls"`
		RenewCalls        uint64 `json:"renewcalls"`
		ReviseCalls       uint64 `json:"revisecalls"`
		SettingsCalls     uint64 `json:"settingscalls"`
		UnrecognizedCalls uint64 `json:"unrecognizedcalls"`
	}

	StorageFolderMetadata struct {
		Capacity          BigNumber `json:"capacity"`
		CapacityRemaining BigNumber `json:"capacityremaining"`
		Index             uint16    `json:"index"`
		Path              string    `json:"path"`
		FailedReads       uint64    `json:"failedreads"`
		FailedWrites      uint64    `json:"failedwrites"`
		SuccessfulReads   uint64    `json:"successfulreads"`
		SuccessfulWrites  uint64    `json:"successfulwrites"`
	}
)
