package types

type (
	FileContractRevision struct {
		ParentID          string           `json:"parentid"`
		UnlockConditions  UnlockConditions `json:"unlockconditions"`
		NewRevisionNumber uint64           `json:"newrevisionnumber"`

		NewFileSize           uint64          `json:"newfilesize"`
		NewFileMerkleRoot     string          `json:"newfilemerkleroot"`
		NewWindowStart        uint64          `json:"newwindowstart"`
		NewWindowEnd          uint64          `json:"newwindowend"`
		NewValidProofOutputs  []SiacoinOutput `json:"newvalidproofoutputs"`
		NewMissedProofOutputs []SiacoinOutput `json:"newmissedproofoutputs"`
		NewUnlockHash         string          `json:"newunlockhash"`
	}

	FileContract struct {
		FileMerkleRoot     string          `json:"filemerkleroot"`
		UnlockHash         string          `json:"unlockhash"`
		FileSize           uint64          `json:"filesize"`
		WindowStart        uint64          `json:"windowstart"`
		WindowEnd          uint64          `json:"windowend"`
		RevisionNumber     uint64          `json:"revisionnumber"`
		Payout             BigNumber       `json:"payout"`
		ValidProofOutputs  []SiacoinOutput `json:"validproofoutputs"`
		MissedProofOutputs []SiacoinOutput `json:"missedproofoutputs"`
	}

	StorageProof struct {
		ParentID string   `json:"parentid"`
		Segment  []byte   `json:"segment"`
		HashSet  []string `json:"hashset"`
	}

	StorageObligation struct {
		ObligationID             string    `json:"obligationid"`
		ObligationStatus         string    `json:"obligationstatus"`
		ContractCost             BigNumber `json:"contractcost"`
		LockedCollateral         BigNumber `json:"lockedcollateral"`
		PotentialDownloadRevenue BigNumber `json:"potentialdownloadrevenue"`
		PotentialStorageRevenue  BigNumber `json:"potentialstoragerevenue"`
		PotentialUploadRevenue   BigNumber `json:"potentialuploadrevenue"`
		RiskedCollateral         BigNumber `json:"riskedcollateral"`
		TransactionFeesAdded     BigNumber `json:"transactionfeesadded"`
		DataSize                 uint64    `json:"datasize"`
		ExpirationHeight         uint64    `json:"expirationheight"`
		NegotiationHeight        uint64    `json:"negotiationheight"`
		ProofDeadLine            uint64    `json:"proofdeadline"`
		SectorRootsCount         uint64    `json:"sectorrootscount"`
		OriginConfirmed          bool      `json:"originconfirmed"`
		ProofConfirmed           bool      `json:"proofconfirmed"`
		ProofConstructed         bool      `json:"proofconstructed"`
		RevisionConfirmed        bool      `json:"revisionconfirmed"`
		RevisionConstructed      bool      `json:"revisionconstructed"`
	}
)
