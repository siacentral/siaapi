package siaapi

import "gitlab.com/siacentral/siaapi/responses"

//GetRawTransaction GetRawTransaction
func (c *Client) GetRawTransaction(id string) (resp responses.TPoolRaw, err error) {
	err = c.makeRequest(HTTPGet, "/tpool/raw/"+id, "", &resp)

	return
}
