package responses

import (
	"gitlab.com/siacentral/siaapi/types"
)

type (
	// Host contains the information that is returned after a GET request to
	// /host - a bunch of information about the status of the host.
	Host struct {
		ExternalSettings     types.HostExternalSettings `json:"externalsettings"`
		FinancialMetrics     types.HostFinancialMetrics `json:"financialmetrics"`
		InternalSettings     types.HostInternalSettings `json:"internalsettings"`
		NetworkMetrics       types.HostNetworkMetrics   `json:"networkmetrics"`
		ConnectabilityStatus string                     `json:"connectabilitystatus"`
		WorkingStatus        string                     `json:"workingstatus"`
	}

	// HostScoreEstimate contains the information that is returned from a
	// /host/estimatescore call.
	HostScoreEstimate struct {
		EstimatedScore types.BigNumber `json:"estimatedscore"`
		ConversionRate float64         `json:"conversionrate"`
	}

	// HostContracts contains the information that is returned after a GET request
	// to /host/contracts - information for the host about stored obligations.
	HostContracts struct {
		Contracts []types.StorageObligation `json:"contracts"`
	}

	// StorageFolders contains the information that is returned after a GET request
	// to /host/storage - a bunch of information about the status of storage
	// management on the host.
	StorageFolders struct {
		Folders []types.StorageFolderMetadata `json:"folders"`
	}
)
