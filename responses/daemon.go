package responses

// DaemonVersion contains information about the running daemon's version.
type DaemonVersion struct {
	Version     string
	GitRevision string
	BuildTime   string
}

// DaemonUpdate contains information about a potential available update for
// the daemon.
type DaemonUpdate struct {
	Available bool   `json:"available"`
	Version   string `json:"version"`
}
