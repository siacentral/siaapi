package responses

import (
	"gitlab.com/siacentral/siaapi/types"
)

type (
	// Wallet contains general information about the wallet.
	Wallet struct {
		Encrypted  bool   `json:"encrypted"`
		Height     uint64 `json:"height"`
		Rescanning bool   `json:"rescanning"`
		Unlocked   bool   `json:"unlocked"`

		ConfirmedSiacoinBalance     types.BigNumber `json:"confirmedsiacoinbalance"`
		UnconfirmedOutgoingSiacoins types.BigNumber `json:"unconfirmedoutgoingsiacoins"`
		UnconfirmedIncomingSiacoins types.BigNumber `json:"unconfirmedincomingsiacoins"`

		SiacoinClaimBalance types.BigNumber `json:"siacoinclaimbalance"`
		SiafundBalance      types.BigNumber `json:"siafundbalance"`

		DustThreshold types.BigNumber `json:"dustthreshold"`
	}

	// Address contains an address returned by a GET call to
	// /wallet/address.
	Address struct {
		Address string `json:"address"`
	}

	//Addresses contains the list of wallet addresses returned by a
	// GET call to /wallet/addresses.
	Addresses struct {
		Addresses []string `json:"addresses"`
	}

	//CreateWallet contains the primary seed that gets generated during a
	// POST call to /wallet/init.
	CreateWallet struct {
		PrimarySeed string `json:"primaryseed"`
	}

	// SentTransactions contains the transaction sent in the POST call to
	// /wallet/siacoins.
	SentTransactions struct {
		TransactionIDs []string `json:"transactionids"`
	}

	// WalletSeeds contains the seeds used by the wallet.
	WalletSeeds struct {
		PrimarySeed        string   `json:"primaryseed"`
		AddressesRemaining int      `json:"addressesremaining"`
		AllSeeds           []string `json:"allseeds"`
	}

	// WalletSweep contains the coins and funds returned by a call to
	// /wallet/sweep.
	WalletSweep struct {
		Coins types.BigNumber `json:"coins"`
		Funds types.BigNumber `json:"funds"`
	}

	// Transaction contains the transaction returned by a call to
	// /wallet/transaction/:id
	Transaction struct {
		Transaction types.ProcessedTransaction `json:"transaction"`
	}

	// Transactions contains the specified set of confirmed and
	// unconfirmed transactions.
	Transactions struct {
		ConfirmedTransactions   []types.ProcessedTransaction `json:"confirmedtransactions"`
		UnconfirmedTransactions []types.ProcessedTransaction `json:"unconfirmedtransactions"`
	}

	// WalletConditions contains a set of unlock conditions.
	WalletConditions struct {
		UnlockConditions types.UnlockConditions `json:"unlockconditions"`
	}

	// WalletUnspent contains the unspent outputs tracked by the wallet.
	// The MaturityHeight field of each output indicates the height of the
	// block that the output appeared in.
	WalletUnspent struct {
		Outputs []types.UnspentOutput `json:"outputs"`
	}

	// VerifyAddress contains a bool indicating if the address passed to
	// /wallet/verify/address/:addr is a valid address.
	VerifyAddress struct {
		Valid bool `json:"valid"`
	}
)
