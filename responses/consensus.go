package responses

import (
	"gitlab.com/siacentral/siaapi/types"
)

// Consensus contains general information about the consensus set
type Consensus struct {
	Synced       bool            `json:"synced"`
	Height       uint64          `json:"height"`
	CurrentBlock string          `json:"currentblock"`
	Tar          []byte          `json:"tar"`
	Difficulty   types.BigNumber `json:"difficulty"`
}

// ConsensusHeaders contains information from a blocks header.
type ConsensusHeaders struct {
	BlockID string `json:"blockid"`
}

// Block contains all fields of a Block and additional
// fields for ID and Height.
type Block struct {
	ID           string                     `json:"id"`
	Height       uint64                     `json:"height"`
	ParentID     string                     `json:"parentid"`
	Nonce        [8]byte                    `json:"nonce"`
	Timestamp    types.Timestamp            `json:"timestamp"`
	MinerPayouts []types.SiacoinOutput      `json:"minerpayouts"`
	Transactions []types.ConsensusBlocksTxn `json:"transactions"`
}
