package responses

import "gitlab.com/siacentral/siaapi/types"

type (
	// Hosts lists active hosts on the network.
	Hosts struct {
		Hosts []types.HostDBEntry `json:"hosts"`
	}

	// HostDetails lists detailed statistics for a particular host, selected
	// by pubkey.
	HostDetails struct {
		Entry          types.HostDBEntry        `json:"entry"`
		ScoreBreakdown types.HostScoreBreakdown `json:"scorebreakdown"`
	}

	// HostScan holds information about the hostdb.
	HostScan struct {
		InitialScanComplete bool `json:"initialscancomplete"`
	}
)
