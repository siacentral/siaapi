## Sia API Client

This is a Go wrapper around the [Sia API](https://sia.tech/docs/). The goal for this wrapper is to be easier to use with less depedencies than the built in API client. 

Most of the method names have been changed and a few additional types have been added which makes this wrapper incompatible with the default wrapper. The naming convention should be a little easier to get around.

Not all methods have been tested, but most of them should work fine. If one doesn't submit a bug or PR and I'll get to it ASAP.

## TODO

+ Better comments
+ Unit Tests
+ Examples