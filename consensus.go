package siaapi

import (
	"net/url"
	"strconv"

	"gitlab.com/siacentral/siaapi/responses"
)

//GetConsensus requests the /consensus api resource
func (c *Client) GetConsensus() (cg responses.Consensus, err error) {
	err = c.makeRequest(HTTPGet, "/consensus", "", &cg)

	return
}

//GetBlockByID requests the /consensus/blocks api resource
func (c *Client) GetBlockByID(id string) (cbg responses.Block, err error) {
	values := url.Values{
		"id": []string{id},
	}

	err = c.makeRequest(HTTPGet, "/consensus/blocks", values.Encode(), &cbg)

	return
}

//GetBlockByHeight requests the /consensus/blocks api resource
func (c *Client) GetBlockByHeight(height uint64) (cbg responses.Block, err error) {
	values := url.Values{
		"height": []string{strconv.FormatUint(height, 10)},
	}

	err = c.makeRequest(HTTPGet, "/consensus/blocks", values.Encode(), &cbg)

	return
}
