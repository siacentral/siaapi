package siaapi

import "gitlab.com/siacentral/siaapi/responses"

// ConnectGateway uses the /gateway/connect/:address endpoint to connect to
// the gateway at address
func (c *Client) ConnectGateway(address string) (err error) {
	_, err = c.makeRawRequest(HTTPPost, "/gateway/connect/"+address, "")

	return
}

// DisconnectGateway uses the /gateway/disconnect/:address endpoint to
// disconnect the gateway from a peer.
func (c *Client) DisconnectGateway(address string) (err error) {
	_, err = c.makeRawRequest(HTTPPost, "/gateway/disconnect/"+address, "")

	return
}

// GetGateways requests the /gateway api resource
func (c *Client) GetGateways() (gwg responses.Gateway, err error) {
	err = c.makeRequest(HTTPGet, "/gateway", "", &gwg)
	return
}
