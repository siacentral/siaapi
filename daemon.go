package siaapi

import (
	"gitlab.com/siacentral/siaapi/responses"
)

// GetDaemonVersion requests the /daemon/version resource
func (c *Client) GetDaemonVersion() (dvg responses.DaemonVersion, err error) {
	err = c.makeRequest(HTTPGet, "/daemon/version", "", &dvg)
	return
}

// StopDaemon stops the daemon using the /daemon/stop endpoint.
func (c *Client) StopDaemon() (err error) {
	_, err = c.makeRawRequest(HTTPGet, "/daemon/stop", "")
	return
}

// CheckForUpdates checks for an available daemon update.
func (c *Client) CheckForUpdates() (dig responses.DaemonUpdate, err error) {
	err = c.makeRequest(HTTPGet, "/daemon/update", "", &dig)
	return
}

// UpdateDaemon updates the daemon.
func (c *Client) UpdateDaemon() (err error) {
	_, err = c.makeRawRequest(HTTPPost, "/daemon/update", "")
	return
}
