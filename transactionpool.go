package siaapi

import (
	"gitlab.com/siacentral/siaapi/responses"
)

// EstimateTPoolFee uses the /tpool/fee endpoint to get a fee estimation.
func (c *Client) EstimateTPoolFee() (tfg responses.TPoolFee, err error) {
	err = c.makeRequest(HTTPGet, "/tpool/fee", "", &tfg)

	return
}
