package siaapi

import (
	"encoding/json"
	"errors"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"

	"gitlab.com/siacentral/siaapi/types"
)

const (
	//HTTPGet HTTPGet
	HTTPGet = HTTPMethod("GET")

	//HTTPPost HTTPPost
	HTTPPost = HTTPMethod("POST")
)

//Client makes requests to the siad HTTP API.
type Client struct {
	// Address is the API address of the siad server.
	Address string

	// Password must match the password of the siad server.
	Password string

	// UserAgent must match the User-Agent required by the siad server. If not
	// set, it defaults to "Sia-Agent".
	UserAgent string
}

//HTTPMethod HTTPMethod
type HTTPMethod string

var client = &http.Client{
	Timeout: time.Minute * 2,
}

// New creates a new Client using the provided address.
func New(address string) *Client {
	return &Client{
		Address: address,
	}
}

func (c *Client) newRequest(method HTTPMethod, resource string, values string) (req *http.Request, err error) {
	var endpoint *url.URL

	if endpoint, err = url.ParseRequestURI("http://" + c.Address); err != nil {
		return
	}

	endpoint.Path = resource

	if method == HTTPGet {
		endpoint.RawQuery = values

		if req, err = http.NewRequest(string(method), endpoint.String(), nil); err != nil {
			return
		}
	} else {
		if req, err = http.NewRequest(string(method), endpoint.String(), strings.NewReader(values)); err != nil {
			return
		}

		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	}

	if len(c.UserAgent) == 0 {
		c.UserAgent = "Sia-Agent"
	}

	req.Header.Set("User-Agent", c.UserAgent)

	if len(c.Password) > 0 {
		req.SetBasicAuth("", c.Password)
	}

	return
}

func drainAndClose(rc io.ReadCloser) {
	io.Copy(ioutil.Discard, rc)
	rc.Close()
}

func readAPIError(r io.Reader) (err error) {
	var apiErr types.ErrorResponse

	if err = json.NewDecoder(r).Decode(&apiErr); err != nil {
		return err
	}

	err = errors.New(apiErr.Message)

	return
}

func (c *Client) makeRawRequest(method HTTPMethod, resource string, values string) (data []byte, err error) {
	var resp *http.Response
	var req *http.Request

	if req, err = c.newRequest(method, resource, values); err != nil {
		return
	}

	if resp, err = client.Do(req); err != nil {
		return
	}

	defer drainAndClose(resp.Body)

	if resp.StatusCode == http.StatusNotFound {
		err = errors.New("api call not recognized: " + req.URL.String())
		return
	}

	if resp.StatusCode < 200 || resp.StatusCode > 299 {
		err = readAPIError(resp.Body)
		return
	}

	if resp.StatusCode == http.StatusNoContent {
		return
	}

	data, err = ioutil.ReadAll(resp.Body)

	return
}

func (c *Client) makeRequest(method HTTPMethod, resource string, values string, obj interface{}) (err error) {
	var data []byte

	if data, err = c.makeRawRequest(method, resource, values); err != nil {
		return
	}

	err = json.Unmarshal(data, &obj)

	return
}
