package requests

type (
	// FilterMode contains the information needed to set the the
	// FilterMode of the hostDB
	FilterMode struct {
		FilterMode string   `json:"filtermode"`
		Hosts      []string `json:"hosts"`
	}
)
