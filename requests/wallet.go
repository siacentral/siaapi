package requests

type (
	// UpdateWatchedAddresses contains the set of addresses to add or remove from the
	// watch set.
	UpdateWatchedAddresses struct {
		Addresses []string `json:"addresses"`
		Remove    bool     `json:"remove"`
		Unused    bool     `json:"unused"`
	}
)
