package siaapi

import (
	"encoding/json"
	"net/url"
	"strconv"

	"gitlab.com/siacentral/siaapi/requests"
	"gitlab.com/siacentral/siaapi/responses"
	types "gitlab.com/siacentral/siaapi/types"
)

// GetNewWalletAddress requests a new address from the /wallet/address endpoint
func (c *Client) GetNewWalletAddress() (wag responses.Address, err error) {
	err = c.makeRequest(HTTPGet, "/wallet/address", "", &wag)

	return
}

// GetWalletAddresses requests the wallets known addresses from the
// /wallet/addresses endpoint.
func (c *Client) GetWalletAddresses() (wag responses.Addresses, err error) {
	err = c.makeRequest(HTTPGet, "/wallet/addresses", "", &wag)
	return
}

// ChangeWalletPassword uses the /wallet/changepassword endpoint to change
// the wallet's password.
func (c *Client) ChangeWalletPassword(currentPassword, newPassword string) (err error) {
	values := url.Values{}

	values.Set("newpassword", newPassword)
	values.Set("encryptionpassword", currentPassword)

	_, err = c.makeRawRequest(HTTPPost, "/wallet/changepassword", values.Encode())

	return
}

// CreateNewWallet uses the /wallet/init endpoint to initialize and encrypt a
// wallet
func (c *Client) CreateNewWallet(password string, force bool) (wip responses.CreateWallet, err error) {
	values := url.Values{}

	values.Set("encryptionpassword", password)
	values.Set("force", strconv.FormatBool(force))

	err = c.makeRequest(HTTPPost, "/wallet/init", values.Encode(), &wip)

	return
}

// RecoverWallet uses the /wallet/init/seed endpoint to initialize and
// encrypt a wallet using a given seed.
func (c *Client) RecoverWallet(seed, password string, force bool) (err error) {
	values := url.Values{}

	values.Set("seed", seed)
	values.Set("encryptionpassword", password)
	values.Set("force", strconv.FormatBool(force))

	_, err = c.makeRawRequest(HTTPPost, "/wallet/init/seed", values.Encode())
	return
}

// GetWallet requests the /wallet api resource
func (c *Client) GetWallet() (wg responses.Wallet, err error) {
	err = c.makeRequest(HTTPGet, "/wallet", "", &wg)

	return
}

// LockWallet uses the /wallet/lock endpoint to lock the wallet.
func (c *Client) LockWallet() (err error) {
	_, err = c.makeRawRequest(HTTPPost, "/wallet/lock", "")

	return
}

// WatchSeed uses the /wallet/seed endpoint to add a seed to the wallet's list
// of seeds.
func (c *Client) WatchSeed(seed, password string) (err error) {
	values := url.Values{}

	values.Set("seed", seed)
	values.Set("encryptionpassword", password)

	_, err = c.makeRawRequest(HTTPPost, "/wallet/seed", values.Encode())

	return
}

// GetWalletSeeds uses the /wallet/seeds endpoint to return the wallet's
// current seeds.
func (c *Client) GetWalletSeeds() (wsg responses.WalletSeeds, err error) {
	err = c.makeRequest(HTTPGet, "/wallet/seeds", "", &wsg)

	return
}

// SendSiacoin uses the /wallet/siacoins api endpoint to send money to a
// single address
func (c *Client) SendSiacoin(amount types.BigNumber, destination string) (wsp responses.SentTransactions, err error) {
	values := url.Values{}

	values.Set("amount", amount.String())
	values.Set("destination", destination)

	err = c.makeRequest(HTTPPost, "/wallet/siacoins", values.Encode(), &wsp)

	return
}

// SendSiafunds uses the /wallet/siafunds api endpoint to send siafunds
// to a single address.
func (c *Client) SendSiafunds(amount types.BigNumber, destination string) (wsp responses.SentTransactions, err error) {
	values := url.Values{}

	values.Set("amount", amount.String())
	values.Set("destination", destination)

	err = c.makeRequest(HTTPPost, "/wallet/siafunds", values.Encode(), &wsp)
	return
}

// SweepWallet uses the /wallet/sweep/seed endpoint to sweep a seed into
// the current wallet.
func (c *Client) SweepWallet(seed string) (wsp responses.WalletSweep, err error) {
	values := url.Values{}

	values.Set("seed", seed)

	err = c.makeRequest(HTTPPost, "/wallet/sweep/seed", values.Encode(), &wsp)

	return
}

// WalletTransactionsGet requests the/wallet/transactions api resource for a
// certain startheight and endheight
func (c *Client) WalletTransactionsGet(startHeight uint64, endHeight uint64) (wtg responses.Transactions, err error) {
	err = c.makeRequest(HTTPGet, "/wallet/transactions", url.Values{
		"startheight": []string{strconv.FormatUint(startHeight, 10)},
		"endheight":   []string{strconv.FormatUint(endHeight, 10)},
	}.Encode(), &wtg)

	return
}

// GetTransactionByID requests the /wallet/transaction/:id api resource for a
// certain TransactionID.
func (c *Client) GetTransactionByID(id string) (wtg responses.Transaction, err error) {
	err = c.makeRequest(HTTPGet, "/wallet/transaction/"+id, "", &wtg)

	return
}

// UnlockWallet uses the /wallet/unlock endpoint to unlock the wallet with
// a given encryption key. Per default this key is the seed.
func (c *Client) UnlockWallet(password string) (err error) {
	values := url.Values{}

	values.Set("encryptionpassword", password)

	_, err = c.makeRawRequest(HTTPPost, "/wallet/unlock", values.Encode())
	return
}

// GetWalletUnlockConditions requests the /wallet/unlockconditions endpoint
// and returns the UnlockConditions of addr.
func (c *Client) GetWalletUnlockConditions(addr string) (wucg responses.WalletConditions, err error) {
	err = c.makeRequest(HTTPGet, "/wallet/unlockconditions/"+addr, "", &wucg)
	return
}

// GetWalletUnspent requests the /wallet/unspent endpoint and returns all of
// the unspent outputs related to the wallet.
func (c *Client) GetWalletUnspent() (wug responses.WalletUnspent, err error) {
	err = c.makeRequest(HTTPGet, "/wallet/unspent", "", &wug)

	return
}

// GetWatchedAddresses requests the /wallet/watch endpoint and returns the set of
// currently watched addresses.
func (c *Client) GetWatchedAddresses() (wwg responses.Addresses, err error) {
	err = c.makeRequest(HTTPGet, "/wallet/watch", "", &wwg)

	return
}

// AddWatchedAddresses uses the /wallet/watch endpoint to add a set of addresses
// to the watch set. The unused flag should be set to true if the addresses
// have never appeared in the blockchain.
func (c *Client) AddWatchedAddresses(addrs []string, unused bool) (err error) {
	var marshalled []byte

	if marshalled, err = json.Marshal(requests.UpdateWatchedAddresses{
		Addresses: addrs,
		Remove:    false,
		Unused:    unused,
	}); err != nil {
		return
	}

	_, err = c.makeRawRequest(HTTPPost, "/wallet/watch", string(marshalled))

	return
}

// RemoveWatchedAddresses uses the /wallet/watch endpoint to remove a set of
// addresses from the watch set. The unused flag should be set to true if the
// addresses have never appeared in the blockchain.
func (c *Client) RemoveWatchedAddresses(addrs []string, unused bool) (err error) {
	var marshalled []byte

	if marshalled, err = json.Marshal(requests.UpdateWatchedAddresses{
		Addresses: addrs,
		Remove:    true,
		Unused:    unused,
	}); err != nil {
		return
	}

	_, err = c.makeRawRequest(HTTPPost, "/wallet/watch", string(marshalled))

	return
}
