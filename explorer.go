package siaapi

import (
	"net/url"

	"gitlab.com/siacentral/siaapi/responses"
)

//GetExplorerHash requests the /consensus api resource
func (c *Client) GetExplorerHash(hash string) (cg responses.ExplorerHash, err error) {
	err = c.makeRequest(HTTPGet, "/explorer/hash", url.Values{
		"hash": []string{hash},
	}.Encode(), &cg)

	return
}
